#include <linux/module.h>
#include <linux/version.h>
#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/kdev_t.h>
#include <linux/fs.h>
#include <linux/device.h>
#include <linux/cdev.h>
#include <linux/uaccess.h>
#include <linux/init.h>
#include <linux/proc_fs.h>
#include <linux/slab.h>
#include <linux/string.h>

#define BUFF_SIZE 1024
#define RESULT_ARRAY_SIZE 1024

static dev_t first;
static struct cdev c_dev; 
static struct class *cl;


static struct proc_dir_entry *entry;
static uint32_t current_index = 0;
static uint32_t result_array[RESULT_ARRAY_SIZE];

static int my_open(struct inode *i, struct file *f)
{
  printk(KERN_INFO "Driver: open()\n");
  return 0;
}

static int my_close(struct inode *i, struct file *f)
{
  printk(KERN_INFO "Driver: close()\n");
  return 0;
}

static ssize_t my_read(struct file *f, char __user *buf, size_t len, loff_t *off)
{
  size_t i = 0;
  printk(KERN_INFO "Driver: read()\n");

  if(*off != 0) {
          printk(KERN_INFO "Can't read from device\n");
          return 0;
  }
  for(; i < current_index; i++) {
          printk("%d", result_array[i]);
  }     
  *off += current_index;
  return 0;
}

static ssize_t my_write(struct file *f, const char __user *buf,  size_t len, loff_t *off)
{
  printk(KERN_INFO "Driver: write()\n");

  if(*off > 0) {
        printk(KERN_INFO "Input massage is too big");
        return 0;
  }
  result_array[current_index++] = len - 1;
  *off += len;
  return len;
}

static const char* const null_str = "NULL string.\n";
static ssize_t proc_read(struct file *f, char __user *buf,  size_t len, loff_t *off)
{
    size_t current_str_len, local_str_len;
    int i;
    char local_str[32];
    char str[1024] = "";
    printk(KERN_INFO "Reading from proc file");

    if (*off != 0) {
        return 0;
    }
    
    if(current_index == 0) {
        current_str_len = strlen(null_str);
        if(copy_to_user(buf, null_str, current_str_len) < 0)
            return -EFAULT;

        *off += current_str_len;
        return current_str_len;
    }
    
    current_str_len = 0;
    for(i=0; i < current_index; i++) {
        local_str_len = sprintf(local_str, "strlen(%1d) = %d\n",i+1, result_array[i]);

        if(local_str_len < 0) {
            return -EFAULT;
        }

        current_str_len += local_str_len;

        if(current_str_len > 1024) {
            return -EFAULT;
        }

        strcat(str, local_str);
    }

    if(copy_to_user(buf, str, current_str_len) < 0)
        return -EFAULT;

    *off += current_str_len;
    return current_str_len;
}

static ssize_t proc_write(struct file *f, const char __user *buf, size_t len, loff_t *off) {
    printk(KERN_DEBUG "Attempt to write into proc file");
    return -1;
}

static struct file_operations mychdev_fops =
{
  .owner = THIS_MODULE,
  .open = my_open,
  .release = my_close,
  .read = my_read,
  .write = my_write
};

static struct file_operations proc_fops = 
{
    .owner = THIS_MODULE,
    .read = proc_read,
    .write = proc_write
};
 
static int __init ch_drv_init(void)
{
    printk(KERN_INFO "Hello!\n");
    if (alloc_chrdev_region(&first, 0, 1, "ch_dev") < 0)
	  {
		return -1;
	  }
    if ((cl = class_create(THIS_MODULE, "chardrv")) == NULL)
	  {
		unregister_chrdev_region(first, 1);
		return -1;
	  }
    if (device_create(cl, NULL, first, NULL, "var1") == NULL)
	  {
		class_destroy(cl);
		unregister_chrdev_region(first, 1);
		return -1;
	  }
    cdev_init(&c_dev, &mychdev_fops);
    if (cdev_add(&c_dev, first, 1) == -1)
	  {
		device_destroy(cl, first);
		class_destroy(cl);
		unregister_chrdev_region(first, 1);
		return -1;
	  }
    
    entry = proc_create("var1", 0666, NULL, &proc_fops);
    if(entry == NULL) {
        cdev_del(&c_dev);
        device_destroy(cl, first);
        class_destroy(cl);
        unregister_chrdev_region(first, 1);
        return -1;
    }

    printk(KERN_INFO "%s: created dev file\n", THIS_MODULE->name);
    printk(KERN_INFO "%s: created proc file\n", THIS_MODULE->name);
    return 0;
}
 
static void __exit ch_drv_exit(void)
{
    cdev_del(&c_dev);
    device_destroy(cl, first);
    class_destroy(cl);
    unregister_chrdev_region(first, 1);
    printk(KERN_INFO "dev file is deleted\n");

    proc_remove(entry);
    printk(KERN_INFO "proc file is deleted\n");

    printk(KERN_INFO "Driver was destroyed\n");
}
 
module_init(ch_drv_init);
module_exit(ch_drv_exit);
 
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Alexander Mashkovtsev, Maxim Admakin");
MODULE_DESCRIPTION("Char driver, 1 lab I/O");

